// Day 6: JavaScript Dates
function getDayName(dateString) {
  const day = new Date(Date.parse(dateString)).getDay();
  const dayName = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ];

  return dayName[day];
}
