// Day 0: Data Types
const secondInteger = "12";
const secondDecimal = "4.32";
const secondString = "is the best place to learn and practice coding!";

function performOperation(secondInteger, secondDecimal, secondString) {
  const firstInteger = 4;
  const firstDecimal = 4.0;
  const firstString = "HackerRank ";

  console.log(Number(firstInteger) + Number(secondInteger));
  console.log(Number(firstDecimal) + Number(secondDecimal));
  console.log(firstString + secondString);
}
