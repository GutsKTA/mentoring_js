// Day 3: Try, Catch, and Finally
const s = "1234";

function reverseString(s) {
  try {
    s = s.split("").reverse().join("");
  } catch (err) {
    console.log(err.message);
  } finally {
    console.log(s);
  }

  // try {
  //   let splitSrt = s.split("");
  //   let reverseArr = splitSrt.reverse();
  //   let joinArr = reverseArr.join("");
  //   console.log(joinArr);
  // } catch (err) {
  //   err = "s.split is not a function";
  //   console.log(err);
  //   console.log(s);
  // }
}
