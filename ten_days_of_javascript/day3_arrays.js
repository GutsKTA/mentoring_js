// Day 3: Arrays
const nums = [2, 3, 6, 6, 5];

function getSecondLargest(nums) {
  const cloneNums = [...new Set(nums)];
  const reverseSorted = cloneNums.sort((a, b) => b - a);
  const second = reverseSorted[1];
  return second;
}
