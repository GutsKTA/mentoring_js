// Day 5: Template Literals
function sides(literals, ...expressions) {
  const sqrt = Math.sqrt(Math.pow(expressions[1], 2) - 16 * expressions[0]);
  const s1 = (expressions[1] + sqrt) / 4;
  const s2 = (expressions[1] - sqrt) / 4;

  return s1 > s2 ? [s2, s1] : [s1, s2];
}
