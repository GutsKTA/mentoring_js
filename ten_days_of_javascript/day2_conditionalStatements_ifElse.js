// Day 2: Conditional Statements: If-Else
const score = 11;

function getGrade(score) {
  const grade = ["F", "E", "D", "C", "B", "A"];
  if (score >= 0 && score <= 5) {
    return grade[0];
  }
  return grade[Math.trunc((score - 1) / 5)];
}
