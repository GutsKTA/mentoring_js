// Day 1: Functions
const n = 4;

function factorial(n) {
  return n > 1 ? n * factorial(n - 1) : 1;

  // let result = n;

  // if (n < 1) {
  //   return 1;
  // }
  // for (var i = n - 1; i >= 1; i--) {
  //   result *= i;
  // }
  // return result;
}
