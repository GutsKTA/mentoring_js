// Day 1: Arithmetic Operators
const length = 3;
const width = 4.5;

function getArea(length, width) {
  return length * width;
}

function getPerimeter(length, width) {
  return 2 * (length + width);
}
