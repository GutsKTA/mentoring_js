// Day 1: Let and Const
function main() {
  const r = readLine();

  const area = Math.PI * Math.pow(r, 2);
  console.log(area);

  const perimeter = 2 * Math.PI * r;
  console.log(perimeter);
}
