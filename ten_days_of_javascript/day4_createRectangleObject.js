// Day 4: Create a Rectangle Object
const a = 4;
const b = 5;

function Rectangle(a, b) {
  this.length = a;
  this.width = b;
  this.perimeter = this.getPerimeter(a, b);
  this.area = this.getArea(a, b);
}

Rectangle.prototype.getPerimeter = (a, b) => 2 * (a + b);
Rectangle.prototype.getArea = (a, b) => a * b;
