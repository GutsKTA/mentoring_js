// Day 2: Loops
const s = "javascriptloops";

function vowelsAndConsonants(s) {
  let result = [];

  for (let letter = 0; letter < s.length; letter++) {
    switch (s[letter]) {
      case "a":
      case "e":
      case "i":
      case "o":
      case "u":
        console.log(s[letter]);
        break;
      default:
        result.push(s[letter]);
    }
  }
  console.log(result.join("\n"));

  // const vowel = s.match(/[aeiou]/gm).join("\n");
  // const nonVowel = s.match(/[^aeiou]/gm).join("\n");
  // console.log(vowel + "\n" + nonVowel);
}
