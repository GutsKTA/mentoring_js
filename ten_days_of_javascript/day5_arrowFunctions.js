// Day 5: Arrow Functions
const nums = [1, 2, 3, 4, 5];

function modifyArray(nums) {
  const arr = (n) => (n = n % 2 === 0 ? n * 2 : n * 3);
  const result = nums.map(arr);
  return result;

  // return nums.map(n => n = (n % 2 === 0) ? n * 2 : n * 3);
}
