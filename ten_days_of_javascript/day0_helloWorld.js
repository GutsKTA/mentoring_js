// Day 0: Hello, World!
const parameterVariable = "Welcome to 10 Days of JavaScript!";

function greeting(parameterVariable) {
  console.log("Hello, World!");
  console.log(parameterVariable);
}
