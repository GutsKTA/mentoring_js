// Day 4: Count Objects
const objects = [
  { x: 1, y: 1 },
  { x: 2, y: 3 },
  { x: 3, y: 3 },
  { x: 3, y: 4 },
  { x: 4, y: 5 },
];

function getCount(objects) {
  return objects.filter((o) => o.x === o.y).length;

  // let count = 0;

  // objects.forEach((o) => {
  //   if (o.x === o.y) {
  //     count++;
  //   }
  // });
  // return count;
}
